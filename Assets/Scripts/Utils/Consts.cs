﻿namespace Utils
{
    public static class Consts
    {
        public const int EMPTY_FIELD = 0;
        public const int DEFAULT_GAME_SIZE = 3;
        public static int RANDOMIZATION_TIME = 200;
    }
}