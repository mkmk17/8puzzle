﻿using UnityEngine;

namespace Utils
{
    //Unlike singleton it is destroyed on load and it assumes that there exists already gameobject on scene.
    //It just ensures that there is only one on scene, and provides simple static access.
    public class WeakSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        private static object _lock = new object();

        public static T Instance
        {
            get
            {
                lock (_lock)
                {

                    if (_instance == null)
                    {
                        _instance = FindObjectOfType<T>();
                    }

                    return _instance;
                }
            }
        }
    }
}