﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Utils {
    public abstract class Pool<T> : MonoBehaviour where T : MonoBehaviour {

        private List<T> _available;
        private List<T> _used;

        protected abstract T Prefab { get; }

        private void Awake() {
            _available = new List<T>();
            _used = new List<T>();
        }

        private T CreateNew(Transform parent, bool worldPositionStays) {
            return Instantiate(Prefab, parent, worldPositionStays);
        }

        public T PoolInstantiate(Action<T> initAction = null, Transform parent = null, bool worldPositionStays = false) {
            T instance;
            if (_available.Count == 0) {
                instance = CreateNew(parent, worldPositionStays);
            }
            else {
                instance = _available[0];
                _available.RemoveAt(0);
            }
            _used.Add(instance);
            instance.gameObject.SetActive(true);
            initAction?.Invoke(instance);
            return instance;
        }

        public void PoolDestroy(T instance) {
            instance.gameObject.SetActive(false);
            _used.Remove(instance);
            _available.Add(instance);
        }

        public void PoolDestroyAll() {
            foreach (T usedInstance in _used) {
                usedInstance.gameObject.SetActive(false);
                _available.Add(usedInstance);
            }
            _used.Clear();
        }
    }
}