﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Assertions;
using static Utils.Consts;

namespace _8Puzzle
{
    /// <summary>
    /// Class representing state of the game.
    /// </summary>
    public class Board
    {
        private List<int> _state = new List<int>();
        private int _size;

        /// <summary>
        /// Take one dimension version of index and transform it to two dimensions version.
        /// </summary>
        /// <param name="singleIndex">One dimension version of index.</param>
        /// <returns>Two dimensions version of index.</returns>
        public (int, int) SingleIndexToDualIndex(int singleIndex)
        {
            return (singleIndex / _size, singleIndex % _size);
        }

        /// <summary>
        /// Take two dimensions version of index and transform it to one dimension version.
        /// </summary>
        /// <param name="dualIndex">Two dimensions version of index.</param>
        /// <returns>One dimension version of index.</returns>
        public int DualIndexToSingleIndex((int, int) dualIndex)
        {
            var (row, column) = dualIndex;
            return row * _size + column;
        }

        /// <summary>
        /// Initialize the board to a solved state.
        /// </summary>
        /// <param name="size">Size of the board. Board is square with sizexsize dimensions.</param>
        public void Init(int size)
        {
            _size = size;
            _state.Clear();
            for (int i = 1; i < size * size; i++)
            {
                _state.Add(i);
            }

            _state.Add(EMPTY_FIELD);
        }

        /// <summary>
        /// Is the board in solved state.
        /// </summary>
        /// <returns>True if board is in solved state, false otherwise.</returns>
        public bool IsSolved()
        {
            for (int i = 0; i < _state.Count - 1; i++)
            {
                if (_state[i] != i+1)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Index of the field that is empty.
        /// </summary>
        /// <returns>Two dimensional index of the field that is empty.</returns>
        private (int, int) EmptyFieldIndex()
        {
            return SingleIndexToDualIndex(_state.IndexOf(EMPTY_FIELD));
        }

        /// <summary>
        /// Is field index valid.
        /// </summary>
        /// <param name="index">Two dimensional index.</param>
        /// <returns>True, if index is valid on the board, false otherwise.</returns>
        private bool IsFieldIndexValid((int, int) index)
        {
            return index.Item1 >= 0 && index.Item1 < _size && index.Item2 >= 0 && index.Item2 < _size;
        }

        /// <summary>
        /// Valid neighbours for given field.
        /// </summary>
        /// <param name="index">Two dimensional index.</param>
        /// <returns>All neighbours of the given field.</returns>
        private IEnumerable<(int, int)> FieldNeighbours((int, int) index)
        {
            return new List<(int, int)>
            {
                (index.Item1 + 1, index.Item2),
                (index.Item1 - 1, index.Item2),
                (index.Item1, index.Item2 + 1),
                (index.Item1, index.Item2 - 1)
            }.Where(IsFieldIndexValid);
        }

        /// <summary>
        /// All of the possible moves in the current state.
        /// </summary>
        /// <returns>All indexes of the fields that can be moved in the current state.</returns>
        public IEnumerable<(int, int)> PossibleMoves()
        {
            //Debug.Log($"Empty field index {EmptyFieldIndex()}");
            //Debug.Log($"Possible moves {string.Join(" ", FieldNeighbours(EmptyFieldIndex()))}");
            //Debug.Log($"Possible moves one index {string.Join(" ", FieldNeighbours(EmptyFieldIndex()).Select(DualIndexToSingleIndex))}");
            return FieldNeighbours(EmptyFieldIndex());
        }

        /// <summary>
        /// Is the move possible in the current state.
        /// </summary>
        /// <param name="index">Two dimensional index.</param>
        /// <returns>True if move is possible, false otherwise.</returns>
        public bool IsMovePossible((int, int) index)
        {
            return PossibleMoves().Contains(index);
        }

        /// <summary>
        /// Make a move.
        /// </summary>
        /// <param name="index">Two dimensional index</param>
        /// <returns>Tuple where first value is the two dimensional index where block was pushed, and second is value of the block.</returns>
        public ((int,int), int) Move((int, int) index)
        {
            Assert.IsTrue(IsMovePossible(index));

            (int, int) emptyFieldIndex = EmptyFieldIndex();
            int switchNo = _state[DualIndexToSingleIndex(index)];
            _state[DualIndexToSingleIndex(index)] = EMPTY_FIELD;
            _state[DualIndexToSingleIndex(emptyFieldIndex)] = switchNo;
            return (emptyFieldIndex, switchNo);
        }

        /// <summary>
        /// Randomize state by making some random moves on current state.
        /// </summary>
        /// <param name="movementNo">Number of moves to make.</param>
        public void Randomize(int movementNo)
        {
            for (int iteration = 0; iteration < movementNo; iteration++)
            {
                List<(int, int)> possibleMoves = PossibleMoves().ToList();
                Move(possibleMoves[Random.Range(0, possibleMoves.Count)]);
            }
        }

        /// <summary>
        /// Current state as a list of values.
        /// </summary>
        /// <returns>Current state as a list of values.</returns>
        public List<int> CurrentState()
        {
            return new List<int>(_state);
        }
    }

}