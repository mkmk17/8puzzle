﻿using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Utils.Consts;

namespace _8Puzzle
{
    /// <summary>
    /// Single element on the puzzle grid.
    /// </summary>
    public class PuzzleElement : MonoBehaviour
    {
        [SerializeField] private Button _button = default;
        [SerializeField] private int _index = default;
        [SerializeField] private TMP_Text _text = default;
        private int _value;

        public void Init(int index)
        {
            _index = index;
        }

        [UsedImplicitly]
        public void OnClick()
        {
            Game.Instance.ButtonPressed(_index);
        }

        public void SetValue(int value)
        {
            _value = value;
            _text.text = value == EMPTY_FIELD ? "" : value.ToString();
        }

        public void SetActive(bool isActive)
        {
            _button.enabled = isActive;
        }
    }
}