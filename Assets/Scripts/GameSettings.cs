﻿using Utils;
using static Utils.Consts;

namespace _8Puzzle
{
    public class GameSettings : Singleton<GameSettings>
    {
        public int GameSize { get; set; }

        private void Awake()
        {
            GameSize = DEFAULT_GAME_SIZE;
        }
    }
}