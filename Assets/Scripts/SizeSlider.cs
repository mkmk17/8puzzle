﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace _8Puzzle
{
    public class SizeSlider : MonoBehaviour
    {
        [SerializeField] private TMP_Text _sizeText = default;
        [SerializeField] private Slider _slider = default;


        private void Awake()
        {
            _slider.value = GameSettings.Instance.GameSize;
        }

        public void OnValueChange()
        {
            int newValue = Mathf.RoundToInt(_slider.value);
            _sizeText.text = newValue.ToString();
            GameSettings.Instance.GameSize = newValue;
        }
    }
}