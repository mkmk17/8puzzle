﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utils;


namespace _8Puzzle
{
    public class GameGrid : WeakSingleton<GameGrid>
    {
        [SerializeField] private PuzzleElementPool _pool = default;
        [SerializeField] private Transform _puzzleElementParent = default;
        [SerializeField] private GridLayoutGroup _grid = default;
        private List<PuzzleElement> _gridElements = new List<PuzzleElement>();

        private void Awake()
        {
            Game.OnGameStarted += Init;
            Game.OnGameEnded += GameOver;
        }

        private void OnDestroy()
        {
            Game.OnGameStarted -= Init;
            Game.OnGameEnded -= GameOver;
        }

        private void Init(int size, List<int> boardState)
        {
            Clean();
            InstantiateGrid(size);

            for (int i = 0; i < boardState.Count; i++)
            {
                SetElementValue(i, boardState[i]);
            }
        }

        private void Clean()
        {
            _pool.PoolDestroyAll();
            _gridElements.Clear();
        }

        private void InstantiateGrid(int size)
        {
            _grid.constraintCount = size;
            for (int i = 0; i < size * size; i++)
            {
                var indexCopy = i;
                _gridElements.Add(_pool.PoolInstantiate((element) => element.Init(indexCopy), _puzzleElementParent));
            }
        }

        public void SetElementValue(int index, int value)
        {
            _gridElements[index].SetValue(value);
        }

        public void UpdateActive(IEnumerable<int> possibleIndexes)
        {
            List<int> possibleIndexesList = possibleIndexes.ToList();
            _gridElements.ForEach(element => element.SetActive(false));
            foreach (var possibleIndex in possibleIndexesList)
            {
                _gridElements[possibleIndex].SetActive(true);
            }
        }

        private void GameOver()
        {
            _gridElements.ForEach(element => element.SetActive(false));
        }
    }
}