﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using static Utils.Consts;

namespace _8Puzzle
{
    /// <summary>
    /// Coordinator of the whole game.
    /// </summary>
    public class Game : WeakSingleton<Game>
    {
        private Board _board;
        [SerializeField] private GameGrid _gameGrid = default;
        [SerializeField] private MovesText _movesText = default;

        public delegate void GameStarted(int size, List<int> boardState);
        public static event GameStarted OnGameStarted;
        public delegate void GameEnded();
        public static event GameEnded OnGameEnded;

        private void Awake()
        {
            _board = new Board();
            StartGame();
        }

        private void UpdateGrid()
        {
            _gameGrid.UpdateActive(_board.PossibleMoves().Select(_board.DualIndexToSingleIndex));
        }

        public void StartGame()
        {
            _board.Init(GameSettings.Instance.GameSize);
            _board.Randomize(RANDOMIZATION_TIME);
            OnGameStarted?.Invoke(GameSettings.Instance.GameSize, _board.CurrentState());
            UpdateGrid();
        }

        public void ButtonPressed(int index)
        {
            if (!_board.IsMovePossible(_board.SingleIndexToDualIndex(index))) return;
            ((int, int) oldEmptyIndex, int switchedValue) = _board.Move(_board.SingleIndexToDualIndex(index));
            _gameGrid.SetElementValue(_board.DualIndexToSingleIndex(oldEmptyIndex), switchedValue);
            _gameGrid.SetElementValue(index, 0);
            UpdateGrid();
            _movesText.MakeMove();
            if (_board.IsSolved())
            {
                OnGameEnded?.Invoke();
            }
        }

        [UsedImplicitly]
        public void Reset()
        {
            StartGame();
        }

        [UsedImplicitly]
        public void ExitToMainMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}