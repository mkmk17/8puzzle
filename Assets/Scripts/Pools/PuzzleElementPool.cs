﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace _8Puzzle
{
    public class PuzzleElementPool : Pool<PuzzleElement>
    {
        [SerializeField] private PuzzleElement _prefab = default;
        protected override PuzzleElement Prefab => _prefab;
    }
}