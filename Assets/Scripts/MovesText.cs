﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace _8Puzzle
{
    public class MovesText : MonoBehaviour
    {
        [SerializeField] private TMP_Text _text = default;
        private int _stepsNo;


        private void Awake()
        {
            Reset();
            Game.OnGameStarted += Reset;
            Game.OnGameEnded += GameOver;
        }

        private void OnDestroy()
        {
            Game.OnGameStarted -= Reset;
            Game.OnGameEnded -= GameOver;
        }

        private void Reset()
        {
            _stepsNo = 0;
            UpdateText();
        }

        public void Reset(int size, List<int> boardState)
        {
            Reset();
        }

        private void UpdateText()
        {
            _text.text = $"Moves: {_stepsNo}";
        }

        private void GameOver()
        {
            _text.text = $"Game won in {_stepsNo} moves!";
        }

        public void MakeMove()
        {
            _stepsNo++;
            UpdateText();
        }
    }
}