﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace _8Puzzle
{
    public class MainMenuButtons : MonoBehaviour
    {
        public void StartGame()
        {
            SceneManager.LoadScene("GameScene");
        }

        public void Exit()
        {
            Application.Quit();
        }
    }
}